FROM openjdk:8-jdk-alpine

COPY . .

CMD ["java", "-jar", "estacionamento-0.0.1-SNAPSHOT.jar"]